package tn.magazinemanagement.ejb.services.interfaces;

import java.util.Calendar;
import java.util.List;

import javax.ejb.Local;

import tn.magazinemanagement.ejb.domain.Article;
import tn.magazinemanagement.ejb.domain.Magazine;
import tn.magazinemanagement.ejb.domain.Redactor;

@Local
public interface MagazineServicesLocal {

	boolean createMagazine(Magazine magazine);

	boolean createRedactor(Redactor redactor);

	boolean createArticle(String nameMagazine, String nameRedactor,
			String lastNameRedactor, String titleArticle,
			Calendar publicationDate);
	
	boolean updateArticle(String title, String label);

	List<Article> findAllArticlesByParams(String cinRedactor,
			String nameRedactor, String lastNameRedactor, String sectionLabel);

}
