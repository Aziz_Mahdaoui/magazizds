package tn.magazinemanagement.ejb.services.imp;

import java.util.Calendar;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import tn.magazinemanagement.ejb.domain.Article;
import tn.magazinemanagement.ejb.domain.ArticlePk;
import tn.magazinemanagement.ejb.domain.Magazine;
import tn.magazinemanagement.ejb.domain.Redactor;
import tn.magazinemanagement.ejb.domain.Section;
import tn.magazinemanagement.ejb.services.interfaces.MagazineServicesLocal;
import tn.magazinemanagement.ejb.services.interfaces.MagazineServicesRemote;

/**
 * Session Bean implementation class MagazineServices
 */
@Stateless
public class MagazineServices implements MagazineServicesRemote,
		MagazineServicesLocal {

	@PersistenceContext
	private EntityManager em;

	/**
	 * Default constructor.
	 */
	public MagazineServices() {
	}

	@Override
	public boolean createMagazine(Magazine magazine) {
		em.persist(magazine);
		return true;
	}

	@Override
	public boolean createRedactor(Redactor redactor) {

		em.persist(redactor);

		return true;

	}

	@Override
	public boolean createArticle(String nameMagazine, String nameRedactor,
			String lastNameRedactor, String titleArticle,
			Calendar publicationDate) {

		Article article = new Article();

		TypedQuery<Magazine> queryMagazine = em.createQuery(
				"SELECT M FROM Magazine M WHERE M.name =:name", Magazine.class);
		queryMagazine.setParameter("name", nameMagazine);

		Magazine magazine = queryMagazine.getSingleResult();

		TypedQuery<Redactor> queryRedactor = em
				.createQuery(
						"SELECT R FROM Redactor R WHERE R.firstName =:firstName AND R.lastName =:lastName",
						Redactor.class);
		queryRedactor.setParameter("firstName", nameRedactor);
		queryRedactor.setParameter("lastName", lastNameRedactor);

		Redactor redactor = queryRedactor.getSingleResult();
		redactor.setCinRedactor(redactor.getCinRedactor());
		redactor.setFirstName(nameRedactor);
		redactor.setLastName(lastNameRedactor);

		ArticlePk articlePk = new ArticlePk(magazine.getIdMagazine(),
				redactor.getCinRedactor(), publicationDate);
		article.setArticlePk(articlePk);
		article.setMagazine(magazine);
		article.setTitre(titleArticle);
		article.setRedactor(redactor);

		em.persist(article);

		return true;
	}

	@Override
	public boolean updateArticle(String title, String label) {

		TypedQuery<Section> querySection = em.createQuery(
				"SELECT S FROM Section S WHERE S.label =:label", Section.class);
		querySection.setParameter("label", label);

		Section section = querySection.getSingleResult();

		TypedQuery<Article> query = em.createQuery(
				"SELECT A FROM Article A WHERE A.titre =:titre", Article.class);
		query.setParameter("titre", title);

		Article article = query.getSingleResult();
		article.setSection(section);

		em.merge(article);

		return true;
	}

	@Override
	public List<Article> findAllArticlesByParams(String cinRedactor,
			String nameRedactor, String lastNameRedactor, String sectionLabel) {

		TypedQuery<Article> query = em
				.createQuery(
						"SELECT A FROM Article A WHERE (A.redactor.cinRedactor = :cinRedactor Or A.redactor.firstName = :nameRedactor) Or (A.redactor.lastName = :lastNameRedactor Or A.section.label = :sectionLabel)",
						Article.class);
		query.setParameter("cinRedactor", cinRedactor);
		query.setParameter("nameRedactor", nameRedactor);
		query.setParameter("lastNameRedactor", lastNameRedactor);
		query.setParameter("sectionLabel", sectionLabel);

		List<Article> ArticleList = query.getResultList();
		return ArticleList;
	}

}
