package tn.magazinemanagement.ejb.domain;

import java.io.Serializable;
import java.util.Calendar;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: ArticlePk
 * 
 */
@Embeddable
public class ArticlePk implements Serializable {

	private static final long serialVersionUID = 1L;
	private int idMagazine;
	private String cinRedactor;
	private Calendar publicationDate;

	public ArticlePk() {
	}

	public ArticlePk(int idMagazine, String cinRedactor,
			Calendar publicationDate) {
		super();
		this.idMagazine = idMagazine;
		this.cinRedactor = cinRedactor;
		this.publicationDate = publicationDate;
	}

	@Column(name = "idMagazinePk")
	public int getIdMagazine() {
		return idMagazine;
	}

	public void setIdMagazine(int idMagazine) {
		this.idMagazine = idMagazine;
	}

	@Column(name = "cinRedactorPk")
	public String getCinRedactor() {
		return cinRedactor;
	}

	public void setCinRedactor(String cinRedactor) {
		this.cinRedactor = cinRedactor;
	}

	public Calendar getPublicationDate() {
		return publicationDate;
	}

	public void setPublicationDate(Calendar publicationDate) {
		this.publicationDate = publicationDate;
	}

	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return super.hashCode();
	}

	@Override
	public boolean equals(Object arg0) {
		// TODO Auto-generated method stub
		return super.equals(arg0);
	}
}
