package tn.magazinemanagement.ejb.domain;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

/**
 * Entity implementation class for Entity: Redactor
 * 
 */
@Entity
public class Redactor implements Serializable {

	private static final long serialVersionUID = 1L;
	private String cinRedactor;
	private String firstName;
	private String lastName;
	private List<Article> articles;

	public Redactor() {
	}

	public Redactor(String cinRedactor, String firstName, String lastName) {
		super();
		this.cinRedactor = cinRedactor;
		this.firstName = firstName;
		this.lastName = lastName;
	}

	public Redactor(String cinRedactor, String firstName, String lastName,
			List<Article> articles) {
		this.cinRedactor = cinRedactor;
		this.firstName = firstName;
		this.lastName = lastName;
		this.articles = articles;
	}

	@Id
	public String getCinRedactor() {
		return cinRedactor;
	}

	public void setCinRedactor(String cinRedactor) {
		this.cinRedactor = cinRedactor;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	@OneToMany(mappedBy = "redactor")
	public List<Article> getArticles() {
		return articles;
	}

	public void setArticles(List<Article> articles) {
		this.articles = articles;
	}

	@Override
	public String toString() {
		return "Redactor [cinRedactor=" + cinRedactor + ", firstName="
				+ firstName + ", lastName=" + lastName + "]";
	}

}
